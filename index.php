<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


	
	
<?php
/**
 * MIT License
 * ===========
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * -----------------------------------------------------------------------
 *
 *
 * Run this in your browser to see the example!
 *
 *
 *
 * IMPORTANT: Clear your sessions/cookies before running UA tests.
 *
 * This is a procedural approach example of how to switch your website layout
 * based on a variable $layoutType.
 *
 * The example also includes the switch links that you can put in the footer
 * of your page. Is a good practice to let the user switch between layouts,
 * even if he is visiting the page from a phone or tablet.
 * ------------------------------------------------------------------------
 *
 * @author      Serban Ghita <serbanghita@gmail.com>
 * @license     MIT License https://github.com/serbanghita/Mobile-Detect/blob/master/LICENSE.txt
 *
 */

// This is mandatory if you're using sessions.
session_start();

// It's mandatory to include the library.
require_once 'assets/mobile_detect.php';

/**
 *  Begin helper functions.
 */

// Your default site layouts.
// Update this array if you have fewer layout types.
function layoutTypes()
{
    return array('classic', 'tablet');

}

function initLayoutType()
{
    // Safety check.
    if (!class_exists('Mobile_Detect')) { return 'classic'; }
	
    $detect = new Mobile_Detect;
    $isMobile = $detect->isMobile();
    $isTablet = $detect->isTablet();

    $layoutTypes = layoutTypes();

    // Set the layout type.
    if ( isset($_GET['layoutType']) ) {

        $layoutType = $_GET['layoutType'];

    } else {

        if (empty($_SESSION['layoutType'])) {

            $layoutType = ($isMobile ? ($isTablet ? 'tablet' : 'mobile') : 'classic');

        } else {

            $layoutType =  $_SESSION['layoutType'];

        }

    }

    // Fallback. If everything fails choose classic layout.
    if ( !in_array($layoutType, $layoutTypes) ) { $layoutType = 'classic'; }

    // Store the layout type for future use.
    $_SESSION['layoutType'] = $layoutType;

    return $layoutType;

}

/**
 *  End helper functions.
 */

// Let's roll. Call this function!
$layoutType = initLayoutType();


?>


	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1"/>
	<meta name="msapplication-tap-highlight" content="no"/>
	<link href='http://fonts.googleapis.com/css?family=Schoolbell' rel='stylesheet' type='text/css'>
	
	<title>Green Schools : Interactive Map</title>
	
	
	<link rel="stylesheet" type="text/css"  href="viewer_skin/global.css"/>
	<script type="text/javascript" src="java/FWDMegazoom.js?vs1"></script>
	
	
	<script type="text/javascript">
		var megazoom;
		
		FWDUtils.onReady(function(){
			megazoom =  new FWDMegazoom({
				//----main----//
				parentId:"myDiv",
				playListAndSkinId:"megazoomPlayList",
				displayType:"fullscreen",
				skinPath:"viewer_skin/skin/",
				imagePath:"images/GreenSchool_Map.jpg",
				preloaderText:"Loading Greenschools",
				useEntireScreen:"yes",
				addKeyboardSupport:"yes",
				addDoubleClickSupport:"yes",
				imageWidth:6000,
				imageHeight:4001,
				zoomFactor:1,
				doubleClickZoomFactor:0.2,
				startZoomFactor:0.4,
				panSpeed:8,
				zoomSpeed:.2,
				backgroundColor:"#D9BA83",
				preloaderFontColor:"#056839",
				preloaderBackgroundColor:"#D9BA83",
				//----lightbox-----//
				lightBoxWidth:800,
				lightBoxHeight:550,
				lightBoxBackgroundOpacity:.8,
				lightBoxBackgroundColor:"#000000",
				//----controller----//
				buttons:"scrollbar",
				buttonsToolTips:"Zoom level:",
				controllerPosition:"bottom",
				inversePanDirection:"yes",
				startSpaceBetweenButtons:10,
				spaceBetweenButtons:10,
				startSpaceForScrollBarButtons:20,
				startSpaceForScrollBar:6,
				hideControllerDelay:100,
				controllerMaxWidth:900,
				controllerBackgroundOpacity:1.0,
				controllerOffsetY:10,
				scrollBarOffsetX:0,
				scrollBarHandlerToolTipOffsetY:4,
				zoomInAndOutToolTipOffsetY:-4,
				buttonsToolTipOffsetY:0,
				hideControllerOffsetY:2,
				buttonToolTipFontColor:"#fff",
				//----navigator----//
				showNavigator:"no",
				showNavigatorOnMobile:"no",
				navigatorImagePath:"",
				navigatorPosition:"topright",
				navigatorOffsetX:6,
				navigatorOffsetY:6,
				navigatorHandlerColor:"#FFFFFF",
				navigatorBorderColor:"#7F3E98",
				//----info window----//
				infoWindowBackgroundOpacity:0.1,
				infoWindowBackgroundColor:"#000000",
				infoWindowScrollBarColor:"#585858",
				//----markers-----//
				showMarkersInfo:"no",
				markerToolTipOffsetY:0,
				markerToolTipOffsetY:2,
				//----context menu----//
				showScriptDeveloper:"no",
				contextMenuLabels:"Zoom in/Zoom out",
				contextMenuBackgroundColor:"#056839",
				contextMenuBorderColor:"#056839",
				contextMenuSpacerColor:"#3AB54A",
				contextMenuItemNormalColor:"#3AB54A",
				contextMenuItemSelectedColor:"#FFFFFF",
				contextMenuItemDisabledColor:"#b7b4b4"
			});
		})
	</script>
		
		
</head>

<body>	

<div style="position:relative;z-index:100000">

<!-- SWITCH LAYOUT PREVIEW  
<?php
/* 
 foreach(layoutTypes() as $_layoutType): ?>
    <?php if($_layoutType == $layoutType): ?>
        <?php echo strtoupper($_layoutType); ?>
    <?php else: ?>
        <a href="<?php echo $_SERVER['PHP_SELF']; ?>?layoutType=<?php echo $_layoutType; ?>"><?php echo strtoupper($_layoutType); ?></a>
    <?php endif; ?>
<?php endforeach;

*/ ?>
--->	
	
<?php 	
if($layoutType == 'classic'):
	$mapMarker= 'viewer_skin/skin/markerInfo.png';
	$mapMarkerHover = 'viewer_skin/skin/markerInfo-rollover.png';
else:
	$mapMarker= 'viewer_skin/skin/markerInfo-rollover.png';
	$mapMarkerHover = 'viewer_skin/skin/markerInfo-rollover.png';
endif;

/* testing
echo $mapMarker;
echo $_layoutType;
echo $layoutType;
*/

?>
</div>


	
	<!-- start viewer, display:none is added in case that js is disabled! -->
	<div id="megazoomPlayList" style="display:none">


	
		<!-- markers -->
		<ul data-markers="">
		
		
			<!-- solar panels, left 5110, top 1765 -->
			<li data-marker-type="infowindow" data-reg-point="centerbottom" data-marker-normal-state-path="<?php echo $mapMarker; ?>" data-marker-selected-state-path="<?php echo $mapMarkerHover; ?>" data-marker-left="5120" data-marker-top="1785" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor="0" data-tool-tip-label="Solar Panels">
				<div class="infoDiv">
					<div class="infoText cf">
					
						
						<div class="infoImg"><figure>
							<img src="images/solar-panels-1.jpg" alt="solar-panels-1" width="302" height="197">
							<figcaption><div class="infoImgSrc"><a href="http://www.nrel.gov/news/features/feature_detail.cfm/feature_id=2012">NREL (National Renewable Energy Laboratory)</a></div></figcaption>
						</figure></div>
						
						<div class="infoContent">
						
						<h1>Solar Panels</h1>
						<p>Harness the sun's power! Solar panels that help power the school not only reduce carbon emissions, but also save money on electricity. Solar systems usually have display readouts so you can see how much electricity is produced on a given day – and how much money and carbon your school saved. For more information, see <a class="link" href="http://www.ases.org/2013/04/solar-for-schools/" target="_blank">Solar for Schools</a>.</p>
						
						</div>
						
					</div>
				</div>
			</li>
			
			
			<!-- skylights, left 4095, top 1071 -->
			<li data-marker-type="infowindow" data-reg-point="centerbottom" data-marker-normal-state-path="<?php echo $mapMarker; ?>" data-marker-selected-state-path="<?php echo $mapMarkerHover; ?>" data-marker-left="4095" data-marker-top="1121" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor="0" data-tool-tip-label="Skylights">
				<div class="infoDiv">
					<div class="infoText cf">
					
						
						<div class="infoImg"><figure>
							<img src="images/skylights-1.jpg" alt="skylights-1" width="307" height="320">
							<figcaption><div class="infoImgSrc"><a href="http://www.boora.com/index.php/projects/k-12-schools/clackamas_high_school">Boora Architects</a></div></figcaption>
						</figure></div>
						
						<div class="infoContent">
						
						<h1>Skylights</h1>
						<p>Here comes the sun! Skylights are often a smart addition to school buildings because they bring in natural light, lowering the need for electric lights. When located in the right place, they also let the sun warm the air inside, reducing winter heating costs. For more information, see <a class="link" href="http://energy.gov/energysaver/articles/skylights">Skylights at Energy.gov</a>.</p>
						
						</div>
						
					</div>
				</div>
			</li>
			
			
			<!-- butterfly garden, 2061, 2484 -->
			<li data-marker-type="infowindow" data-reg-point="centerbottom" data-marker-normal-state-path="<?php echo $mapMarker; ?>" data-marker-selected-state-path="<?php echo $mapMarkerHover; ?>" data-marker-left="2081" data-marker-top="2494" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor="0" data-tool-tip-label="Butterfly Garden">
				<div class="infoDiv">
					<div class="infoText cf">
					
						
						<div class="infoImg"><figure>
							<img src="images/butterfly-garden2.jpg" alt="butterfly-garden2" width="307" height="214">
							<figcaption><div class="infoImgSrc"><a href="http://dirtgarden.wordpress.com/projects/butterfly-garden-at-cathedral-montessori-school/">Dirt Garden </a></div></figcaption>
						</figure></div>
						
						<div class="infoContent">
							<h1>Butterfly Garden</h1>
							<p>Who doesn't love butterflies? These showy insects are important pollinators, helping plants to thrive. You can attract butterflies to your school by growing a butterfly garden with a variety of plants they love. Monarch butterflies, for example, need milkweed plants to lay their eggs. For more information, see <a class="link" href="http://butterflywebsite.com/butterflygardening.cfm" target="_blank">Butterfly Gardening</a>.</p>
						</div>
					</div>
				</div>
			</li>
			
			
			<!-- cork/bamboo flooring, 4015, 1836 -->
			<li data-marker-type="infowindow" data-reg-point="centerbottom" data-marker-normal-state-path="<?php echo $mapMarker; ?>" data-marker-selected-state-path="<?php echo $mapMarkerHover; ?>" data-marker-left="4015" data-marker-top="1836" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor="0" data-tool-tip-label="Cork or
bamboo flooring">
				<div class="infoDiv">
					<div class="infoText cf">
						
						
						<div class="infoImg"><figure>
							<img src="images/flooring-1.jpg" alt="flooring-1" width="285" height="191">
							<figcaption><div class="infoImgSrc"><a href="http://www.corkfloor.us/golden-beach-cork-tiles.html">Cork Floor US</a></div></figcaption>
						</figure></div>
						
						<div class="infoContent">
						
						<h1>Cork or bamboo flooring</h1>
						<p>Check under your feet!  Believe it or not, some school flooring is "greener" than others. Cork and bamboo are durable and sustainable options. Cork comes from the bark of cork oak trees, and can be collected many times in a tree's life. Bamboo is made from a fast-growing grass plant, which can regrow after the woody stems are harvested. For more information, see <a class="link" href="http://www.greenbuild.org/sustainable-materials/sustainable-building-cork/">Cork Flooring</a>, and <a class="link" href="http://www.greenbuild.org/new-construction/uses-for-bamboo-in-sustainable-building">Uses for Bamboo</a>.</p>
						
						</div>
					</div>
				</div>
			</li>
			
			<!-- no idling sign, 3630, 3030 -->
			<li data-marker-type="infowindow" data-reg-point="centerbottom" data-marker-normal-state-path="<?php echo $mapMarker; ?>" data-marker-selected-state-path="<?php echo $mapMarkerHover; ?>" data-marker-left="3656" data-marker-top="3050" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor="0" data-tool-tip-label="No Idling Sign">
				<div class="infoDiv">
					<div class="infoText cf">
					
						
						<div class="infoImg"><figure>
							<img src="images/no-idling.jpg" alt="no-idling" width="296" height="223">
							<figcaption><div class="infoImgSrc"><a href="http://www.crsd.org/Page/16973">Council Rock School District</a></div></figcaption>
						</figure></div>
						
						<div class="infoContent">
						
						<h1>No Idling Sign</h1>
						<p>Quit your idling! Idling is when a driver keeps a parked car's engine running. Near schools, idling exposes children to unhealthy exhaust that can affect the lung, eye, throat, and nasal passages – and is especially harmful to people with asthma. A school "no idling" policy helps to keep air clean. For more information, see the <a class="link" href="http://www.earthday.org/noidling" target="_blank">Earth Day Organization's No Idling Campaign.</a></p>
						
						</div>
						
					</div>
				</div>
			</li>
			
			
			<!-- clothing recyle bin, 790, 2321 -->
			<li data-marker-type="infowindow" data-reg-point="centerbottom" data-marker-normal-state-path="<?php echo $mapMarker; ?>" data-marker-selected-state-path="<?php echo $mapMarkerHover; ?>" data-marker-left="880" data-marker-top="2351" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor="0" data-tool-tip-label="Clothing Recycle">
				<div class="infoDiv">
					<div class="infoText cf">
					
						
						<div class="infoImg"><figure>
							<img src="images/clothes-recycle1.jpg" alt="clothes-recycle1" width="266" height="235">
							<figcaption><div class="infoImgSrc"><a href="http://melroserecycles.wordpress.com/2013/05/14/school-textile-recycling-program-to-benefit-ptos/">Melrose Recycles</a></div></figcaption>
						</figure></div>
						
						<div class="infoContent">
					
						<h1>Clothing Recycle Bin</h1>
						<p>Think before you toss! Every year, Americans throw away millions of tons of clothing and shoes – valuable resources that could be used by people in need or recycled. Clothes recycling bins in schools help to reduce both waste and greenhouse gases. If all those clothes and shoes were recycled, it would be similar to removing a million cars from America's roads. For more information, see <a class="link" href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1964887/" target="_blank">Waste Couture</a>.</p>
						
						</div>
						
					</div>
				</div>
			</li>
			
			
			<!-- compost bins, 1205, 2429 -->
			<li data-marker-type="infowindow" data-reg-point="centerbottom" data-marker-normal-state-path="<?php echo $mapMarker; ?>" data-marker-selected-state-path="<?php echo $mapMarkerHover; ?>" data-marker-left="1285" data-marker-top="2439" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor="0" data-tool-tip-label="Compost Bins">
				<div class="infoDiv">
					<div class="infoText cf">
					
						
						<div class="infoImg"><figure>
							<img src="images/compost-2.jpg" alt="compost-2" width="300" height="225">
							<figcaption><div class="infoImgSrc"><a href="http://www.mansfieldct.org/mms-compost/benefits.htm">Mansfield Middle School Composting program</a></div></figcaption>
						</figure></div>
						
						<div class="infoContent">
					
						<h1>Compost Bins</h1>
						<p>Break it down! Composting is a natural process that uses fungi, bacteria, and other microorganisms to break down food waste and other organic material, making a rich, soil fertilizer (humus). Composting bins speed up this process and help schools reduce trash, conserve space, and save money. For more information see <a class="link" href="http://www.greenmountainfarmtoschool.org/wp/wp-content/uploads/Guide-to-Staring-a-School-Compost-Program.pdf" target="_blank">A Guide to Starting a Composting Program in Your School."</a></p>
						
						</div>
						
					</div>
				</div>
			</li>
			
			
			<!-- rain barrel 2174, 2680 -->
			<li data-marker-type="infowindow" data-reg-point="centerbottom" data-marker-normal-state-path="<?php echo $mapMarker; ?>" data-marker-selected-state-path="<?php echo $mapMarkerHover; ?>" data-marker-left="2214" data-marker-top="2700" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor="0" data-tool-tip-label="Rain Barrel">
				<div class="infoDiv">
					<div class="infoText cf">
					
						
						<div class="infoImg"><figure>
							<img src="images/rain-barrel1.jpg" alt="rain-barrel1" width="300" height="262">
							<figcaption><div class="infoImgSrc"><a  href="http://www.aquabarrel.com/learn_paint_a_barrel.php">Aqua Barrels</a></div></figcaption>
						</figure></div>
						
						<div class="infoContent">
					
						<h1>Rain Barrel</h1>
						<p>Water, water, everywhere! You can save water, help the environment, make your plants happy – and have fun creating art at school with rain barrels. Rain barrels store rain water for later use. If you recycle plastic drums used for food storage, you can also reduce waste. For more information, see <a class="link" href="http://www.stroudcenter.org/education/projects/rain_barrels.shtm" target="_blank">The Rain Barrel Project</a>.</p>
						
						</div>
						
					</div>
				</div>
			</li>
			
			
			

			
			
			
			<!--
			<li data-marker-type="tooltip" data-show-content="yes" data-reg-point="centerbottom" data-marker-normal-state-path="viewer_skin/skin/marker1.png" data-marker-selected-state-path="viewer_skin/skin/marker1-rollover.png" data-marker-left="1536" data-marker-top="270" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor="0">
				<div class="groenlandToolTipInfoDiv">
					<div>
						<img class="groenland1Image" src="viewer_skin/css_graphics/groenland1.png" width="83" height="124"/>
						<p class="groenland1P">This type of tooltip windows can be added with ease, they have a responsive layout and CSS support <a class="link" href="http://www.google.com" target="_blank">external link</a>.</p>
					</div>
					<div>
						<p class="groenland2P">The <span class="dark">hotspots</span> / <span class="dark">markers</span> can be of any size or shape (jpg, gif or png).</p>
						<img class="groenland2Image" src="viewer_skin/css_graphics/groenland2.png" width="202" height="97"/>
					</div>
				</div>
			</li>
			
			<li data-marker-type="tooltip" data-show-content="yes" data-reg-point="centerbottom" data-marker-normal-state-path="viewer_skin/skin/marker1.png" data-marker-selected-state-path="viewer_skin/skin/marker1-rollover.png" data-marker-left="2377" data-marker-top="1966" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor="0">
				<div class="africaToolTipInfoDiv">
					<img style="display:block;" src="viewer_skin/css_graphics/africa.png" width="234" height="260"/>
					<p class="africaP">The <span class="dark">hotspots</span> / <span class="dark">markers</span> can be of any size or shape (jpg, gif or png).</p>
				</div>
			</li>
			
			<li data-marker-type="tooltip" data-show-content="yes" data-reg-point="center" data-marker-normal-state-path="viewer_skin/skin/marker4.png" data-marker-selected-state-path="viewer_skin/skin/marker4-rollover.png" data-marker-left="3667" data-marker-top="2041" data-marker-width="26" data-marker-height="26" data-show-after-zoom-factor="0">
				<div class="australiaToolTipInfoDiv">
					<img src="viewer_skin/css_graphics/australia.png" width="270" height="211"/>
					<p class="australiaP">This type of tooltip windows can be added with ease, they have a responsive layout and CSS support <a class="link" href="http://www.google.com" target="_blank">external link example</a>.  The hotspots / markers can be of any size or shape and of course custom graphics can be used (.jpg or .png).</p>
				</div>
			</li>	
			
			<li data-marker-type="tooltip" data-show-content="yes" data-reg-point="centerbottom" data-marker-normal-state-path="viewer_skin/skin/marker1.png" data-marker-selected-state-path="viewer_skin/skin/marker1-rollover.png" data-marker-left="1768" data-marker-top="707" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor=".65">
				<div class="islandToolTipInfoDiv">
					<img src="viewer_skin/css_graphics/island.png" width="186" height="143"/>
					<p class="islandP">This type of tooltip windows can be added with ease, they have a responsive layout and CSS support <a class="link" href="http://www.google.com" target="_blank">external link example</a>.  The hotspots / markers can be of any size or shape and of course custom graphics can be used (.jpg or .png).</p>
				</div>
			</li>
			
			
			<li data-marker-type="infowindow" data-reg-point="centerbottom" data-marker-normal-state-path="viewer_skin/skin/markerInfo.gif"  data-marker-selected-state-path="<?php echo $mapMarkerHover; ?>" data-marker-left="3320" data-marker-top="1389" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor="0" data-tool-tip-label="Large content window (support for unlimited text)">
				<div class="infoDiv">
					<h1 class="largeLabel">LOREM IPSUM DOLOR SIT AMET</h1>
					<img class="leftImage" src="viewer_skin/css_graphics/camera1.png" width="198" height="137">
					<p class="leftImageParagraph"><span class="boldDark">This type of window support unlimited text</span>, if the html content is too large on a mouse enabled device a scrollbar will appear and if the device has touch support the html content can be scrolled with the finger. This window has a responsive layout this means that it will adapt based on the available space (resize the browser window to see this feature in action). <a class="link" href="http://www.google.com" target="_blank">external link</a> amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra est eu neque feugiat molestie. Sed nec laoreet ligula. Nulla cursus sapien ac massa ultrices id placerat massa varius.Nunc ac turpis nulla. Vestibulum placerat metus urna.</p>
					<div class="separator"></div>
					<h1 class="largeLabel">LOREM IPSUM DOLOR SIT AMET</h1>
					<img class="rightImage" src="viewer_skin/css_graphics/camera2.png" width="198" height="137"/>
					<p class="rightImageParagraph"><span class="boldDark">Lorem ipsum dolor sit amet</span>, consectetur adipiscing elit. Aenean interdum enim eu ligula volutpat nec imperdiet nisi faucibus. Donec est diam, congue sed dapibus non, <a class="link" href="http://www.google.com" target="_blank">external link</a> rhoncus id felis. Nullam aliquam leo vel sem blandit sit amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra est eu neque feugiat molestie. Sed nec laoreet ligula. Nulla cursus sapien ac massa ultrices id placerat massa varius. Nunc ac turpis nulla. Vestibulum placerat metus urna. Suspendisse leo purus, euismod vitae sollicitudin vitae, viverra nec eros orem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<div class="columnsSeparator"></div>
					<div class="columns">
						<p class="columnsFirstParagraph"><span class="boldDark">Lorem ipsum dolor sit amet</span>, consectetur adipiscing elit. Aenean interdum enim eu ligula volutpat nec imperdiet nisi faucibus. Donec est diam, congue sed dapibus non, rhoncus id felis. Nullam aliquam leo vel sem blandit sit amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra est eu neque feugiat molestie. Sed nec laoreet ligula.</p>
						<p><span class="boldDark">Lorem ipsum dolor sit amet</span>, consectetur adipiscing elit. Aenean interdum enim eu ligula volutpat nec imperdiet nisi faucibus. Donec est diam, congue sed dapibus non, rhoncus id felis. Nullam aliquam leo vel sem blandit sit amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra est eu neque feugiat molestie. Sed nec laoreet ligula.</p>
						<p class="columnsLastParagraph"><span class="boldDark">Lorem ipsum dolor sit amet</span>, consectetur adipiscing elit. Aenean interdum enim eu ligula volutpat nec imperdiet nisi faucibus. Donec est diam, congue sed dapibus non, rhoncus id felis. Nullam aliquam leo vel sem blandit sit amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra est eu neque feugiat molestie. Sed nec laoreet ligula.</p>	
					</div>
					<div class="separator"></div>
					<h1 class="largeLabel">LOREM IPSUM DOLOR SIT AMET</h1>
					<iframe  class="youtubeVideo" src="http://www.youtube.com/embed/tC6uAfGun54?wmode=opaque" frameborder="0" allowfullscreen></iframe>
					<p class="lastParagraph"><span class="boldDark">Lorem ipsum dolor sit amet</span>, consectetur adipiscing elit. Aenean interdum enim eu ligula volutpat nec imperdiet nisi faucibus. Donec est diam, congue sed dapibus non, rhoncus id felis. Nullam aliquam leo vel sem blandit sit <a class="link" href="http://www.google.com" target="_blank">external link</a> amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra est eu neque feugiat molestie. Sed nec laoreet ligula. Nulla cursus sapien ac massa ultrices id placerat massa varius. Nunc ac turpis nulla. Vestibulum placerat metus urna. Suspendisse leo purus, euismod vitae sollicitudin vitae, viverra nec eros orem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum enim eu ligula volutpat nec imperdiet nisi faucibus. Donec est diam, congue sed dapibus non, rhoncus id felis. Nullam aliquam leo vel sem blandit sit amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra.</p>
				</div>
			</li>
			
			<li data-marker-type="infowindow" data-reg-point="centerbottom" data-marker-normal-state-path="<?php echo $mapMarker; ?>" data-marker-selected-state-path="<?php echo $mapMarkerHover; ?>" data-marker-left="1337" data-marker-top="2034" data-marker-width="70" data-marker-height="70" data-show-after-zoom-factor="0" data-tool-tip-label="Large content window (support for unlimited text)">
				<div class="infoDiv">
					<h1 class="largeLabel">LOREM IPSUM DOLOR SIT AMET</h1>
					<img class="leftImage" src="viewer_skin/css_graphics/camera1.png" width="198" height="137">
					<p class="leftImageParagraph"><span class="boldDark">This type of window support unlimited text</span>, if the html content is too large on a mouse enabled device a scrollbar will appear and if the device has touch support the html content can be scrolled with the finger. This window has a responsive layout this means that it will adapt based on the available space (resize the browser window to see this feature in action). <a class="link" href="http://www.google.com" target="_blank">external link</a> amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra est eu neque feugiat molestie. Sed nec laoreet ligula. Nulla cursus sapien ac massa ultrices id placerat massa varius.Nunc ac turpis nulla. Vestibulum placerat metus urna.</p>
					<div class="separator"></div>
					<h1 class="largeLabel">LOREM IPSUM DOLOR SIT AMET</h1>
					<img class="rightImage" src="viewer_skin/css_graphics/camera2.png" width="198" height="137"/>
					<p class="rightImageParagraph"><span class="boldDark">Lorem ipsum dolor sit amet</span>, consectetur adipiscing elit. Aenean interdum enim eu ligula volutpat nec imperdiet nisi faucibus. Donec est diam, congue sed dapibus non, <a class="link" href="http://www.google.com" target="_blank">external link</a> rhoncus id felis. Nullam aliquam leo vel sem blandit sit amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra est eu neque feugiat molestie. Sed nec laoreet ligula. Nulla cursus sapien ac massa ultrices id placerat massa varius. Nunc ac turpis nulla. Vestibulum placerat metus urna. Suspendisse leo purus, euismod vitae sollicitudin vitae, viverra nec eros orem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<div class="columnsSeparator"></div>
					<div class="columns">
						<p class="columnsFirstParagraph"><span class="boldDark">Lorem ipsum dolor sit amet</span>, consectetur adipiscing elit. Aenean interdum enim eu ligula volutpat nec imperdiet nisi faucibus. Donec est diam, congue sed dapibus non, rhoncus id felis. Nullam aliquam leo vel sem blandit sit amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra est eu neque feugiat molestie. Sed nec laoreet ligula.</p>
						<p><span class="boldDark">Lorem ipsum dolor sit amet</span>, consectetur adipiscing elit. Aenean interdum enim eu ligula volutpat nec imperdiet nisi faucibus. Donec est diam, congue sed dapibus non, rhoncus id felis. Nullam aliquam leo vel sem blandit sit amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra est eu neque feugiat molestie. Sed nec laoreet ligula.</p>
						<p class="columnsLastParagraph"><span class="boldDark">Lorem ipsum dolor sit amet</span>, consectetur adipiscing elit. Aenean interdum enim eu ligula volutpat nec imperdiet nisi faucibus. Donec est diam, congue sed dapibus non, rhoncus id felis. Nullam aliquam leo vel sem blandit sit amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra est eu neque feugiat molestie. Sed nec laoreet ligula.</p>	
					</div>
					<div class="separator"></div>
					<h1 class="largeLabel">LOREM IPSUM DOLOR SIT AMET</h1>
					<iframe  class="youtubeVideo" src="http://www.youtube.com/embed/tC6uAfGun54?wmode=opaque" frameborder="0" allowfullscreen></iframe>
					<p class="lastParagraph"><span class="boldDark">Lorem ipsum dolor sit amet</span>, consectetur adipiscing elit. Aenean interdum enim eu ligula volutpat nec imperdiet nisi faucibus. Donec est diam, congue sed dapibus non, rhoncus id felis. Nullam aliquam leo vel sem blandit sit <a class="link" href="http://www.google.com" target="_blank">external link</a> amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra est eu neque feugiat molestie. Sed nec laoreet ligula. Nulla cursus sapien ac massa ultrices id placerat massa varius. Nunc ac turpis nulla. Vestibulum placerat metus urna. Suspendisse leo purus, euismod vitae sollicitudin vitae, viverra nec eros orem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum enim eu ligula volutpat nec imperdiet nisi faucibus. Donec est diam, congue sed dapibus non, rhoncus id felis. Nullam aliquam leo vel sem blandit sit amet tincidunt ligula semper. Sed luctus lorem dui, ut lobortis diam. Curabitur est sapien, viverra et aliquet ut, semper nec magna. In molestie, leo a ornare mollis, orci lacus fermentum felis, a scelerisque ante urna tincidunt diam. Ut pharetra.</p>
				</div>
			</li>
			-->
			
			<!-- video example
			<li data-marker-type="infowindow" data-reg-point="center" data-marker-normal-state-path="viewer_skin/skin/marker4.gif" data-marker-selected-state-path="viewer_skin/skin/marker4-rollover.png" data-marker-left="2069" data-marker-top="1246" data-marker-width="26" data-marker-height="26" data-show-after-zoom-factor="0" data-tool-tip-label="Video example">
				
				<div class="infoDiv">
					<h1>Video Embed Example</h1>
					<iframe width="640" height="360" src="//www.youtube.com/embed/HD4bpztESWw?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</li>
			-->
			
		</ul>
	</div>
</body>
</html>




